# Meetup Maio 2019

## Palestras

[Abertura - Jorge Clécio](https://devopspbs.gitlab.io/meetups/meetup-2019-05-25/)

[Primeiros Passos Com Docker - Dhony Silva e Tiago Rocha](https://devopspbs.gitlab.io/talk-intro-docker/)

O que é o DevSecOps - Thiago Rodrigues

SCRUM - Hélio Bentzen

Desenvolvimento nativo para Android com Adroid Studio - Willian Begot

> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
